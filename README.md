PROYECTO UMOVIL
===============

1. Descripcion del Proyecto
------------------------
UMóvil es una la iniciativa de un proyecto de emprendimiento que implica el
desarrollo de una aplicación para dispositivos móviles (celulares y/o tabletas)
que permita el control de asistencia, toma de controles de lecturas y 
exposiciones en vivo de los estudiantes universitarios como herramienta de 
apoyo a los docentes de la universidad nacional y privada.

2. Objetivos
-------------
Generar aplicación móvil para las universidades ubicadas en ciudad de Huacho y 
Huaura que posteriormente puedan ser replicada en otras universidades.

3. Oportunidad
--------------
Existe un crecimiento geométrico del  desarrollo de las aplicaciones móviles 
evidenciados en la cantidad y calidad de productos disponibles a través de las 
diferentes plataformas: Apple Store, Google Play entre otras, en ese contexto 
una aplicación móvil para la docentes universitarios potenciara los servicios 
ofrecidos a los estudiantes agregando el componente de movilidad para permitir
un mejor flexibilidad y optimización de tiempos en tareas complementarias para 
rubricas de evaluación del estudiante.

4. Detalle
----------
Curso: Desarrollo Emprendor.
Plataforma: Android.
Universidad: Jose Faustino Sanchez Carrion .
Facultad: Ingenieria De Sistemas.
Desarrollado por: Equipo IX Ciclo / 2018-II.
Docente: Adolfo Galindo Santiago.

